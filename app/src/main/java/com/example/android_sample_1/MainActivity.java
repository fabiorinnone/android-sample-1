package com.example.android_sample_1;

import android.app.Activity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set activity layout
        setContentView(R.layout.activity_main);

        TextView helloWorldTextView = findViewById(R.id.hello_world_text_view);
        TextView pippoTextView = findViewById(R.id.pippo_text_view);

        helloWorldTextView.setText("Prova");
    }
}
